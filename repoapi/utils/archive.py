import tarfile

import zstandard


def tar_zst_extract_single(stream, filename, closefd=True):
    dctx = zstandard.ZstdDecompressor()
    with dctx.stream_reader(stream, closefd=closefd) as df:
        with tarfile.open(fileobj=df, mode="r:") as tf:
            while ti := tf.next():
                if ti.isfile() and ti.name == filename:
                    return tf.extractfile(ti).read()
