def parse_package_fullname(fullname: str):
    name, ver, rel, arch = fullname.rsplit('-', maxsplit=3)
    ver = f"{ver}-{rel}"
    return name, ver, arch


def parse_pkginfo(lines):
    obj = {}
    for line in lines:
        if line.startswith('#'):
            continue
        k, v = (x.strip() for x in line.split("=", maxsplit=1))
        if k in {"conflict", "depend", "makedepend", "optdepend"}:
            k += "s"
        match k:
            case "conflict" | "replaces" | "provides" | "depends" | "makedepends":
                dep = {}
                v = v.split('=', maxsplit=1)
                dep['name'] = v[0]
                if len(v) > 1:
                    dep['ver'] = v[1]
                obj.setdefault(k, []).append(dep)
            case "optdepends":
                optdep = {}
                v = v.split(':', maxsplit=1)
                optdep['pkgname'] = v[0]
                if len(v) > 1:
                    optdep['reason'] = v[1].strip()
                obj.setdefault(k, []).append(optdep)
            case "backup":
                obj.setdefault(k, []).append(v)
            case _:
                obj[k] = v

    return obj


def parse_buildinfo(lines):
    obj = {}
    for line in lines:
        if line.startswith('#'):
            continue
        k, v = (x.strip() for x in line.split("=", maxsplit=1))
        match k:
            case "buildenv" | "options":
                obj.setdefault(k, []).append(v)
            case "installed":
                name, ver, arch = parse_package_fullname(v)
                obj.setdefault(k, []).append({
                    "pkgname": name,
                    "arch": arch,
                    "pkgver": ver,
                })
            case _:
                obj[k] = v

    return obj
