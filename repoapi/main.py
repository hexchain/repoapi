from fastapi import FastAPI
from fastapi.responses import PlainTextResponse

from .routers import archweb

app = FastAPI()
app.include_router(archweb.router, prefix="/archweb")


@app.get("/", response_class=PlainTextResponse, include_in_schema=False)
async def root():
    return "RepoAPI"
