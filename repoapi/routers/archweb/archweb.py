import logging

import requests
from fastapi import APIRouter, HTTPException

from . import utils
from ...utils import archive, packages
from ...schemas import BuildInfo, PkgInfo

ARCHWEB_BASE = "https://archlinux.org"
MIRROR_ARCH = "x86_64"
MIRRORS = [
    "https://cloudflaremirrors.com/archlinux/",
    "https://mirror.pkgbuild.com/",
]

router = APIRouter()
logger = logging.getLogger(__name__)
session = requests.Session()


@router.on_event("startup")
def startup():
    session.stream = True
    session.headers['Accept-Encoding'] = 'identity'


@router.get("/packages/{repo}/{arch}/{name}/buildinfo",
            response_model=BuildInfo,
            response_model_exclude_unset=True)
def buildinfo(repo: str, arch: str, name: str):
    file = utils.get_package_file(session, ARCHWEB_BASE, MIRRORS, MIRROR_ARCH, repo,
                                  arch, name)
    if not file:
        raise HTTPException(status_code=404, detail="no available mirrors")

    try:
        content = archive.tar_zst_extract_single(file, ".BUILDINFO")
    except KeyError:
        raise HTTPException(status_code=404, detail="invalid package")

    return packages.parse_buildinfo(content.decode().splitlines())


@router.get("/packages/{repo}/{arch}/{name}/pkginfo",
            response_model=PkgInfo,
            response_model_exclude_unset=True)
def pkginfo(repo: str, arch: str, name: str):
    file = utils.get_package_file(session, ARCHWEB_BASE, MIRRORS, MIRROR_ARCH, repo,
                                  arch, name)
    if not file:
        raise HTTPException(status_code=404, detail="no available mirrors")

    try:
        content = archive.tar_zst_extract_single(file, ".PKGINFO")
    except KeyError:
        raise HTTPException(status_code=404, detail="invalid package")

    return packages.parse_pkginfo(content.decode().splitlines())
