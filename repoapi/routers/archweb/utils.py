import logging
from urllib.parse import urljoin

import requests

from repoapi.schemas.archweb import ArchwebPackage

logger = logging.getLogger(__name__)


def get_package_file(session: requests.Session,
                     archweb_base, mirrors, mirror_arch, repo, arch, name):
    r = session.get(urljoin(archweb_base, f"/packages/{repo}/{arch}/{name}/json"))
    if r.status_code == 404:
        return

    r.raise_for_status()

    pkg = ArchwebPackage.parse_obj(r.json())
    q: requests.Response | None = None
    for mirror in mirrors:
        try:
            q = session.get(
                urljoin(mirror, f"{pkg.repo}/os/{mirror_arch}/{pkg.filename}"),
                stream=True)
            q.raise_for_status()
            break
        except requests.exceptions.HTTPError as e:
            logger.info(
                f"package {pkg.pkgname} ({pkg.filename}) not found at {mirror}: "
                f"status {e.response.status}")
            continue

    return q.raw if q else None

