from pydantic import BaseModel


class ArchwebPackage(BaseModel):
    pkgname: str
    pkgbase: str
    repo: str
    arch: str
    pkgver: str
    pkgrel: str
    epoch: int
    pkgdesc: str
    url: str
    filename: str
    compressed_size: int
    installed_size: int
    build_date: str
    last_update: str
    flag_date: str | None
    maintainers: list[str]
    packager: str
    groups: list
    licenses: list[str]
    conflicts: list[str]
    provides: list[str]
    replaces: list[str]
    depends: list[str]
    optdepends: list[str]
    makedepends: list[str]
    checkdepends: list[str]
