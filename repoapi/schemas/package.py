from pydantic import BaseModel


class InstalledItem(BaseModel):
    pkgname: str
    arch: str
    pkgver: str


class BuildInfo(BaseModel):
    format: str
    pkgname: str
    pkgbase: str
    pkgver: str
    pkgarch: str
    pkgbuild_sha256sum: str
    packager: str
    builddate: str
    builddir: str
    startdir: str
    buildtool: str
    buildtoolver: str
    buildenv: list[str]
    options: list[str]
    installed: list[InstalledItem]


class PackageLikeItem(BaseModel):
    name: str
    ver: str | None = None


class OptionalDependItem(BaseModel):
    pkgname: str
    reason: str | None = None


class PkgInfo(BaseModel):
    pkgname: str
    pkgbase: str
    pkgver: str
    pkgdesc: str
    url: str
    builddate: str
    packager: str
    size: str
    arch: str
    license: str
    replaces: list[PackageLikeItem] | None = None
    conflict: list[PackageLikeItem] | None = None
    provides: list[PackageLikeItem] | None = None
    backup: list[str] | None = None
    depends: list[PackageLikeItem] | None = None
    optdepends: list[OptionalDependItem] | None = None
    makedepends: list[PackageLikeItem] | None = None
