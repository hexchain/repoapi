#!/usr/bin/python

import argparse
import contextlib
import io
import logging
import multiprocessing
import stat
import tarfile
import tempfile
from pathlib import Path
from typing import IO

import colorlog
import sh
import zstandard
from elftools.common.exceptions import ELFError
from elftools.elf.dynamic import DynamicSection
from elftools.elf.elffile import ELFFile

colorlog.basicConfig()
logger = logging.getLogger(__name__)


def parse_package_fullname(fullname: str):
    name, ver, rel, arch = fullname.rsplit('-', maxsplit=3)
    ver = f"{ver}-{rel}"
    return name, ver, arch


def find_library_deps(file: IO[bytes]):
    sodeps = set()
    elf = ELFFile(file)
    for section in elf.iter_sections():
        if not isinstance(section, DynamicSection):
            continue
        for tag in section.iter_tags():
            if tag.entry.d_tag == "DT_NEEDED":
                sodeps.add((tag.needed, elf.elfclass))
    return sodeps


def find_package_sodeps(tar: tarfile.TarFile):
    sodeps = set()
    while info := tar.next():
        try:
            if not (info.isfile() and (info.mode & stat.S_IXUSR)):
                continue
            content = tar.extractfile(info).read()
            sodeps |= find_library_deps(io.BytesIO(content))
        except tarfile.ReadError as e:
            logger.error("error while reading file: %s", e)
            break
        except ELFError as e:
            pass
    return sodeps


@contextlib.contextmanager
def tarfile_open(filename: str):
    with contextlib.ExitStack() as stack:
        f = stack.enter_context(open(filename, "rb"))
        if filename.endswith(".zst"):
            decompressor = zstandard.ZstdDecompressor()
            f = stack.enter_context(decompressor.stream_reader(f))
            mode = "r:"
        elif filename.endswith(".tar"):
            mode = "r:"
        else:
            mode = "r"
        yield stack.enter_context(tarfile.open(fileobj=f, mode=mode))


def process_one_file(pkgfile: str, output: str):
    output = Path(output)
    pkgfile = Path(pkgfile)
    name = pkgfile.name[:pkgfile.name.index(".pkg.tar")]
    pkgname, ver, _ = parse_package_fullname(name)
    try:
        with tarfile_open(str(pkgfile.absolute())) as tar:
            sodeps = find_package_sodeps(tar)
    except (tarfile.ReadError, zstandard.ZstdError):
        logger.exception(f"error processing file {pkgfile}")
        return

    subdir = output / f"{pkgname}-{ver}"
    subdir.mkdir()
    links_file = subdir / "links"
    if sodeps:
        sodeps = sorted((x[0] for x in sodeps), key=str.lower)
        links_file.write_text('\n'.join(sodeps) + '\n')


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--filename", help="package filename")
    parser.add_argument("-d", "--repo-path", help="path to repository")
    parser.add_argument("-o", "--output", help="links database output file name")
    args = parser.parse_args()
    if args.filename:
        with tarfile_open(args.filename) as tar:
            sodeps = find_package_sodeps(tar)
            print('\n'.join(sorted((x[0] for x in sodeps), key=str.lower)))
    elif args.repo_path:
        if not args.output:
            logger.error("output file name not specified")
            exit(1)

        repo = Path(args.repo_path)
        if not repo.exists() or not repo.is_dir():
            logger.error("repo path does not exist or is not a directory")
            exit(1)

        with tempfile.TemporaryDirectory() as tempdir:
            with multiprocessing.Pool() as p:
                p.starmap(
                    process_one_file,
                    ((str(x.absolute()), tempdir)
                     for x in repo.glob("*.pkg.tar*")
                     if not x.name.endswith(".sig")))

            sh.tar("--sort=name", "-C", tempdir,
                   "-caf", Path(args.output).absolute(), ".")


if __name__ == "__main__":
    main()
